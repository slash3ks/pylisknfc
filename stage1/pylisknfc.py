#!/usr/bin/env python
# -*- coding: utf8 -*-

import sys
import signal
import argparse
import RPi.GPIO as GPIO
import MFRC522

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal, frame):
    """
    """
    MIFAREReader.MFRC522_StopCrypto1()
    GPIO.cleanup()
    sys.exit()

def data_write_setup():
    """
    """
    text = ['one','two','three','four','five','six',
            'seven','eight','nine','ten','eleven','twelve']

    text_data = []
    data = bytearray(" ".join(text))
    data_iterations = len(data) / 16 + 1
    for i in range(0, data_iterations):
        text_data.append(data[(i*16):(i+1)*16].ljust(16, '\0'))

    return text_data

def main(mfrc522, writeable_blocks, text_data, execution_mode):
    """
    """
    continue_reading = True

    while continue_reading:
    
        # Scan for cards
        (status, TagType) = mfrc522.MFRC522_Request(mfrc522.PICC_REQIDL)

        # If we have find a card, time to party
        if status == mfrc522.MI_OK:

            for idx, data_block in enumerate(writeable_blocks):

                print idx, data_block
                
                for key in MFRC522.MIFARE_CLASSIC_1K_KEYS:

                    # Scan the card again
                    (status, TagType) = mfrc522.MFRC522_Request(mfrc522.PICC_REQIDL)

                    # Get the UID of the card
                    (status, uid) = mfrc522.MFRC522_Anticoll()

                    # If we have the UID, continue
                    if status == mfrc522.MI_OK:

                        # Select the scanned tag
                        mfrc522.MFRC522_SelectTag(uid)

                        # Authenticate
                        status = mfrc522.MFRC522_Auth(mfrc522.PICC_AUTHENT1A,
                                                      data_block,
                                                      key,
                                                      uid)

                        # Check if authenticated
                        if status == mfrc522.MI_OK:

                            # If reading or writing I will collect the current data
                            data = mfrc522.MFRC522_Read(data_block)

                            if execution_mode == 'write':

                                # TODO clean this ugly thing up
                                try:
                                    _ = text_data[idx]
                                except IndexError:
                                    print "Done Writing"
                                    end_read(None, None)
                                    
                                # Write the data
                                mfrc522.MFRC522_Write(data_block, text_data[idx])
                            
                            elif execution_mode == 'read':

                                # TODO modify MRFC522 so it returns a list instead of a string
                                text_data.append("".join([chr(x) for x in eval(data['data'])]))

                                if idx == 11:
                                    print text_data
                                    print "".join(text_data)
                                    end_read(None, None)

                            mfrc522.MFRC522_StopCrypto1()

def usage():
    """
    """

    return '''
    pylisknfc.py --execution-mode read

    pylisknfc.py --execution-mode write
    '''

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='pylisknfc.py', usage=usage())
    
    PARSER.add_argument('-e',
                        '--execution-mode',
                        dest='execution_mode',
                        action='store',
                        choices=['write', 'read'],
                        help='')
    
    ARGS = PARSER.parse_args()

    # Hook the SIGINT
    signal.signal(signal.SIGINT, end_read)

    # Create an object of the class MFRC522
    MIFAREReader = MFRC522.MFRC522()

    # Writeable blocks for this example
    WRITEABLE_BLOCKS = [4,5,6,8,9,10,12,13,14,16,17,18]

    
    if ARGS.execution_mode == 'write':
        TEXTDATA = data_write_setup()
    else:
        TEXTDATA = []

    main(MIFAREReader, WRITEABLE_BLOCKS, TEXTDATA, ARGS.execution_mode)
