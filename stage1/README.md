##### Stage 1 PoC

![writer](writer.png)
![reader](reader.png)
![offline](offline_tx.png)

1. Write the 1st passphrase to a nfc card/tag
2. Write the 2nd passphrase to a different nfc card/tag
3. Read the passphrase(s) from the tag and use it to generate a transaction offline
  * For #3:
    * Need gui or cli to enter destination and amount (cli in MVP)
    * Need to know if the account has a second passphrase
    * Transaction needs to be turned into a QR code to scan with a phone and broadcast (pyliskcrypt + qrcodelib)
