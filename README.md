# pylisknfc
Lisk NFC/RFID related ramblings and PoCs

# Links

[Mifare MF1 IC S50](https://cdn-shop.adafruit.com/datasheets/S50.pdf)

[RPi/Arduino Sensor Module Kit](https://www.amazon.com/Gowoops-RFID-Kit-Arduino-Raspberry/dp/B01KFM0XNG)

[NDEF Decoder and Encoder for Python](https://ndeflib.readthedocs.io/en/stable/index.html)

[MFRC522-python](https://github.com/mxgxw/MFRC522-python)

##### Stage 1 PoC

Generate a transaction offline.

##### Stage 2
